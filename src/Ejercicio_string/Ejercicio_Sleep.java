package Ejercicio_string;

import java.time.Duration;
import java.time.Instant;

public class Ejercicio_Sleep {
	
	public static int i = 0;
	public static void main(String[] args) {
		long startTime = System.nanoTime();
		Instant start = Instant.now();
		while(i <= 100) {
			System.out.println(i);
			i++;
			
			try {
				Thread.sleep(30);
			}catch(InterruptedException e){
				e.printStackTrace();
			}
		}
		
		for(int i = 0; i <= 50; i++) {
			System.out.println(i);
			
			
			try {
				Thread.sleep(30);
			}catch(InterruptedException e){
				e.printStackTrace();
			}
		}
		long endTime = System.nanoTime(); 
		Instant end = Instant.now();
		Duration Interval = Duration.between(start, end);
		
		long TimeElapsed = startTime - endTime;
		System.out.println("Segundos: " +Interval.getSeconds());
	}

}
