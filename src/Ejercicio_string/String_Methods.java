package Ejercicio_string;


public class String_Methods {
	
	public static void Ejemplo1 (String s1) {
		s1 = s1 + "Ejemplo1";
	}

	public static void Ejemplo2 (StringBuilder s2) {
		s2.append("Ejempo2");
	}
	
	public static void Ejemplo2(StringBuffer s3) {
		s3.append("Ejemplo3");
	}

	public static void main(String[] args) {
		String s1="Ejemplo";
		Ejemplo1(s1);
		System.out.println("String: "+s1);
		
		//Metodos StringBuffer
		
		StringBuilder s2 = new StringBuilder("Texto agregado ");
		Ejemplo2(s2);
		System.out.println("StrindBuilder: "+s2);
		
		StringBuffer s3 = new StringBuffer("Texto agreado 2 ");
		Ejemplo2(s3);
		System.out.println("StringBuffer: "+s3);
		
		String str  = "Ejemplo";
		
		StringBuffer sb = new StringBuffer(str);
		sb.reverse();
		System.out.println(sb);
		
		StringBuffer str1 = new StringBuffer("Ejemplo Insert");
		//str1.insert(8, "con ");
		//System.out.println(str1.toString());
		
		//str1.replace(8, 14, "replace");
		//System.out.println(str1.toString());
		
		str1.delete(1, 4);
		System.out.println(str1.toString());
		
		//Conteo de cadena de caracteres
		
		String cadena = new String("Cadena de Texto");
		System.out.println("El tama�o de " + cadena + " es de " + cadena.length() + " caracteres");
		
		//Metodo Concat
		
		String s = "Texto 1";
		s = s.concat(" Texto 2");
		System.out.println(s);
		
		String mensaje = "texto";
		int num1 = 34;
		double pi = Math.PI;
		
		System.out.println(String.format("cadena: %s numero: %d numero pi: %.12f",mensaje,num1,pi));
	
		
	}

}
