package Ejercicio_string;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime; 
import java.time.format.DateTimeFormatter; 
import java.util.Calendar; 
import java.util.Date;
import java.util.GregorianCalendar;
public class Ejercicio_Date {
		
		private static final DateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss"); 
		private static final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		
		public static void main(String[] args) throws Exception { 
			
		Date date = new Date(); System.out.println(sdf.format(date)); 
		Calendar cal = Calendar.getInstance(); System.out.println(sdf.format(cal.getTime())); 
		LocalDateTime now = LocalDateTime.now(); System.out.println(dtf.format(now)); 
		LocalDate localDate = LocalDate.now(); 
		System.out.println(DateTimeFormatter.ofPattern("yyy/MM/dd").format(localDate)); 
		System.out.println("----------------------------------------------------");
		
		//Convertir String a Date
		String sDate1 = "Thu, Dec 31 1998";
		
		SimpleDateFormat Formatter = new SimpleDateFormat("E, MMM dd yyyy");
		Date date1 = Formatter.parse(sDate1);
		System.out.println(date1);
		
		//GregorianCalendar
		Date f = new Date();
		GregorianCalendar Gca = new GregorianCalendar();
		Gca.setTime(f);
		System.out.println("ERA: "+ Gca.get(Calendar.ERA));
		System.out.println("YEAR: "+ Gca.get(Calendar.YEAR));
		System.out.println("MONTH: "+ Gca.get(Calendar.MONTH));
		
		
		ByteArrayOutputStream bOutput = new ByteArrayOutputStream(12);

	      while( bOutput.size()!= 15 ) {
	         // Gets the inputs from the user
	         bOutput.write("hello".getBytes());  
	      }
	      byte b [] = bOutput.toByteArray();
	      System.out.println("Print the content");
	      
	      for(int x = 0; x < b.length; x++) {
	         // printing the characters
	         System.out.print((char)b[x]  + "   "); 
	      }
	      System.out.println("   ");

	      int c;
	      ByteArrayInputStream bInput = new ByteArrayInputStream(b);
	      System.out.println("Converting characters to Upper case " );
	      
	      for(int y = 0 ; y < 1; y++ ) {
	         while(( c = bInput.read())!= -1) {
	            System.out.println(Character.toUpperCase((char)c));
	         }
	         bInput.reset(); 
	      }

	}

}
