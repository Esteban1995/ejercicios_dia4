package Ejercicio_string;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import javax.swing.JOptionPane; 
public class Files1 { 
	public static void main(String[] args) throws  Exception { 
		 File currentDir = new File(".");
	      displayDirectoryContents(currentDir);
	   } 
	   public static void displayDirectoryContents(File dir) {
	      try { 
	         File[] files = dir.listFiles();
	         for (File file : files) {
	            if (file.isDirectory()) {
	               System.out.println("directory:" + file.getCanonicalPath());
	               displayDirectoryContents(file);
	            } else {
	               System.out.println("     file:" + file.getCanonicalPath());
	            } 
	         } 
	      } catch (IOException e) {
	         e.printStackTrace();
	      } 
		
//		String nombre;
//		String codigo;
//		String sueldo;
//		String bonos;
//		String descuento;		
//		
//		 codigo = JOptionPane.showInputDialog("Ingrese el codigo");
//		 nombre = JOptionPane.showInputDialog("Ingrese el nombre");
//		 sueldo = JOptionPane.showInputDialog("Ingrese el sueldo");
//		 bonos = JOptionPane.showInputDialog("Ingrese el bono");
//		 descuento = JOptionPane.showInputDialog("Ingrese el descuento");
//	
//		 String[] array = new String[5];
//		    
//		    array[0] = codigo;
//		    array[1] = nombre;
//		    array[2] = "$ "+sueldo;
//		    array[3] = "$ "+bonos;
//		    array[4] = "$ "+descuento;
//
//		    // 2. Escribir el arreglo al archivo.
//		    writeArray(array);
//
//		    // 3. Leer el arreglo del archivo.
//		    String[] deserializedArray = readArray();
//
//		    // 4. Verificar contenidos del arreglo.
//		    {
//		        for (String item : array) {
//		            System.out.println(item);
//		        }
//		    }
//		}
//
//		private static void writeArray(String[] array) throws IOException {
//		    try (ObjectOutputStream out = new ObjectOutputStream(
//		            new FileOutputStream("continuo.csv"))) {
//
//		        out.writeObject(array);
//		    }
//		}
//
//		private static String[] readArray() throws IOException, ClassNotFoundException {
//		    try (ObjectInputStream in = new ObjectInputStream(
//		            new FileInputStream("continuo.csv"))) {
//
//		        return (String[]) in.readObject();
//		    }
		}
		
        
}